const Range = ({label,value,getRange,handleClick}) => 
{
   
	return(
            <div>
                <label>
                  {label}
                  <input type="range" id="vol" name="vol" min="0" max="50" onChange={getRange} />
                </label>
                <label>
                    {value !=='' ?<h6>Range value is: {value}</h6> : ''} 
                </label>
                
            </div>
        )
}

export default Range;