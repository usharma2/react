const Page = ({placeholder,value,handleChange,handleSubmit}) => 

{

	return <div>
        <label>
          Name:
          <input type="text" placeholder={placeholder} value={value} onChange={handleChange}/> 
		{value !=='' ?<h2>Input value is: {value}</h2> : ''} 
        </label>
        <button onClick={handleSubmit}>submit</button></div>
      
}





export default Page;
