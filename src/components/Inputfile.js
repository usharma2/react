import React from 'react';


const Inputfile = ({placeholder,value,handleChange,getFile,filename}) => 
{
	return(
            <div>
                <label>
                    Name: 
                    <input type="text" placeholder={placeholder} value={value} onChange={handleChange}/> 
		            <br></br>
                </label>
                <label>
                    File: 
                    <input type="file" placeholder={placeholder} onChange={getFile}/> 
		                
                </label>
                <label>
                    {value !=='' ?<h6>Name : {value}</h6> : ''} 
                    {filename !=='' ?<h6>Selected image path is: {filename}</h6> : ''} 
                </label>
            </div>
        )
}

export default Inputfile;