import React from 'react';
const options = [
    {
      label: "MBA",
      value: "MBA",
    },
    {
      label: "MCA",
      value: "MCA",
    },
    {
      label: "Medical",
      value: "Medical",
    },
    
  ];

const SelectOption = ({label,value,getDepartment}) => 
{
   
	return(
            <div>
                <label>
                    {label}
                    <select onChange={getDepartment}>
                        {options.map((option) => (
                        <option value={option.value}>{option.label}</option>
                        ))}
                    </select>
                </label>
                <label>
                    {value !=='' ?<h6>Department value is: {value}</h6> : ''} 
                </label>
               
            </div>
        )
}

export default SelectOption;