import React from 'react';
const RadioButton = ({label,getGender,value}) => 
{
   
	return(
            <div>
                <label>
                    {label}
                    <div onChange={getGender}>
                        <input type="radio" value="Male" name="gender" /> Male
                        <input type="radio" value="Female" name="gender" /> Female
                        
                    </div>
                </label>
                <label>
                    {value !=='' ?<h6>Selected Gender is: {value}</h6> : ''} 
                </label>
               
            </div>
        )
}

export default RadioButton;