const InputText = ({placeholder,value,handleChange}) => 
{
	return <div>
			<input type="text" placeholder={placeholder} value={value} onChange={handleChange}/> 
				{value !=='' ?<h2>Input value is: {value}</h2> : ''} 
			</div>
}



export default InputText;
