import React from 'react';


const Date = ({placeholder,value,getDate,handleSubmit}) => 
{
   
	return(
            <div>
                <label>
                    DOB: 
                    <input type="date" value={value} placeholder={placeholder} onChange={getDate}/> 
                </label>
                <label>
                    {value !=='' ?<h6>DOB value is: {value}</h6> : ''} 
                </label>
               
            </div>
        )
}

export default Date;