import React, { useState, useEffect } from 'react';

import './App.css';


import Test from './components/Test'; 
import Helloworld from './components/Helloworld'; 
import Button from './components/Button'; 

import InputText from './components/InputText'; 
import Page from './components/Page'; 
import TextInput from './components/TextInput'; 


const App = () => 
{
  const [state, setText] = useState('22');
  const handleChange = (event) => 
  {
    setText(event.target.value);
  };


  const handleSubmit = () => 
  {
    console.log(state);
  };

  return (
      <div className="App">
      <Helloworld />
      <Test name="Welcome" />
      <Button label="Click me" />
      <InputText placeholder="Enter number" value={state} handleChange={handleChange} />

      <TextInput placeholder="Enter number" value={state} handleChange={handleChange} />

      
      <Page placeholder="Enter number" value={state} handleChange={handleChange} handleSubmit={handleSubmit} />
      </div>
  );
}


export default App;

      