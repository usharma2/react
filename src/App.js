import React, { useState, useEffect } from 'react';
import './App.css';
import Test from './components/Test'; 
import Helloworld from './components/Helloworld'; 
import Button from './components/Button'; 
import InputText from './components/InputText'; 
import Page from './components/Page'; 
import TextInput from './components/TextInput'; 
import Inputfile from './components/Inputfile'; 
import Date from './components/Date'; 
import SelectOption from './components/SelectOption'; 
import RadioButton from './components/RadioButton'; 
import Range from './components/Range'; 

const App = () => 
{
  
  const [state, setText] = useState('');
  const [name, setName] = useState('');
  const [file, setFile] = useState('');
  const [dob, setDob] = useState('');
  const [range, setRange] = useState('');
  const [department,setDepartment] = useState('');
  const [gender, setGender] = useState('');
  const [flag, setFlag] = useState(false);
  const handleChange = (event) => 
  {
    setText(event.target.value);
  };
  
  const handleSubmit = () => 
  {
    console.log(state);
  };

  const getName = (event) =>
  {
    setFlag(false);
    setName(event.target.value)
  }

  const getFile = (event) =>{
      setFlag(false);
      setFile(event.target.value);
  }

  const getDate = (event) => {
    setFlag(false);
    setDob(event.target.value);
  }

  const getDepartment = (event) => {
    setFlag(false);
    setDepartment(event.target.value);
  }

  const getGender = (event) => {
    setFlag(false);
    setGender(event.target.value);
  }

  const getRange = (event) => {
    setFlag(false);
    setRange(event.target.value);
  }
 
  const handleClick = (event) =>{
    event.preventDefault();
    setFlag(true);
  }

  return (
      <div className="App">
        {/* <Helloworld />
        <Test name="Welcome" />
        
        <InputText placeholder="Enter number" value={state} handleChange={handleChange} />
        <TextInput placeholder="Enter number" value={state} handleChange={handleChange} />
        <Page placeholder="Enter number" value={state} handleChange={handleChange} 
            handleSubmit={handleSubmit} /> */}
        <Inputfile placeholder="Enter Name" value={state} handleChange={handleChange} 
            getFile={getFile} filename={file} />
        <Date placeholder="Enter DOB" value={dob} getDate={getDate} />
        <SelectOption label="Select Department" options={department} getDepartment={getDepartment} value={department} />
        <RadioButton label="Select Gender" getGender={getGender} value={gender} />
        <Range label="Range" getRange={getRange} value={range}  />
        <Button handleClick={handleClick} />
          {flag ===true ? 
            <div> 
              Name: {state} <br/> 
              File: {file} <br/> 
              DOB: {dob} <br/> 
              Occupation: {department} <br/> 
              Gender: {gender} <br/> 
              Range: {range} <br/> 
            </div> : ''} 
        </div>
  );
}

export default App;
